import QtQuick 2.15

/**
 * This is quick and dirty model for the keys of the InputPanel *
 * The code has been derived from
 * http://tolszak-dev.blogspot.de/2013/04/qplatforminputcontext-and-virtual.html
 * Copyright 2015 Uwe Kindler
 * Licensed under MIT see LICENSE.MIT in project root
 */
Item {
    property QtObject firstRowModel: first
    property QtObject secondRowModel: second
    property QtObject thirdRowModel: third


    ListModel {
        id:first
        ListElement { letter: "q"; firstSymbol: "1"; secondSymbol: "1"; keycode: Qt.Key_Q}
        ListElement { letter: "w"; firstSymbol: "2"; secondSymbol: "2"; keycode: Qt.Key_W}
        ListElement { letter: "e"; firstSymbol: "3"; secondSymbol: "3"; keycode: Qt.Key_E}
        ListElement { letter: "r"; firstSymbol: "4"; secondSymbol: "4"; keycode: Qt.Key_R}
        ListElement { letter: "t"; firstSymbol: "5"; secondSymbol: "5"; keycode: Qt.Key_T}
        ListElement { letter: "y"; firstSymbol: "6"; secondSymbol: "6"; keycode: Qt.Key_Y}
        ListElement { letter: "u"; firstSymbol: "7"; secondSymbol: "7"; keycode: Qt.Key_U}
        ListElement { letter: "i"; firstSymbol: "8"; secondSymbol: "8"; keycode: Qt.Key_I}
        ListElement { letter: "o"; firstSymbol: "9"; secondSymbol: "9"; keycode: Qt.Key_O}
        ListElement { letter: "p"; firstSymbol: "0"; secondSymbol: "0"; keycode: Qt.Key_P}
    }
    ListModel {
        id:second
        ListElement { letter: "a"; firstSymbol: "@"; secondSymbol: "£";}
        ListElement { letter: "s"; firstSymbol: "#"; secondSymbol: "¥";}
        ListElement { letter: "d"; firstSymbol: "€"; secondSymbol: "$";}
        ListElement { letter: "f"; firstSymbol: "%"; secondSymbol: "¢";}
        ListElement { letter: "g"; firstSymbol: "&"; secondSymbol: "^";}
        ListElement { letter: "h"; firstSymbol: "-"; secondSymbol: "°";}
        ListElement { letter: "j"; firstSymbol: "+"; secondSymbol: "=";}
        ListElement { letter: "k"; firstSymbol: "("; secondSymbol: "{";}
        ListElement { letter: "l"; firstSymbol: ")"; secondSymbol: "}";}
    }
    ListModel {
        id:third
        ListElement { letter: "z"; firstSymbol: "*"; secondSymbol: "\\";}
        ListElement { letter: "x"; firstSymbol: "\""; secondSymbol: "©";}
        ListElement { letter: "c"; firstSymbol: "'"; secondSymbol: "®";}
        ListElement { letter: "v"; firstSymbol: ":"; secondSymbol: "™";}
        ListElement { letter: "b"; firstSymbol: ";"; secondSymbol: "℅";}
        ListElement { letter: "n"; firstSymbol: "!"; secondSymbol: "[";}
        ListElement { letter: "m"; firstSymbol: "?"; secondSymbol: "]";}
    }
}
